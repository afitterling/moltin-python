# setup

- pip install virtualenv
- cd {REPO_DIR}
- python3 -m virtualenv .
- pip install -r requirements.txt

# activate
- cd {REPO_DIR}
- source bin/activate
- export CLIENT_ID=xxxxxxx
- export CLIENT_SECRET=xxxxxxx
- jupyter-notebook nbs/

# freeze
- pip freeze -l > requirements.txt 
